/**
* (c) Copyright 2019 CERN                                                     
*                                                                             
* This software is distributed under the terms of the GNU General Public      
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   
*                                                                             
* In applying this licence, CERN does not waive the privileges and immunities 
* granted to it by virtue of its status as an Intergovernmental Organization  
* or submit itself to any jurisdiction.                                       
*/

import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Student, CombinationService } from '../services/combination.service';
import { MatSliderModule } from '@angular/material/slider';
import {MatFormFieldModule} from '@angular/material/form-field';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentStudent = {} as Student;
  isComplete = false;
  authStatus: boolean;
  authForm: FormGroup;
  formBuilder = new FormBuilder;

  constructor( private combinationService: CombinationService ) {}

  onSubmit(){
    this.currentStudent.firstname = this.authForm.controls.firstname.value;
    this.currentStudent.surname = this.authForm.controls.surname.value;
    this.currentStudent.grade = this.authForm.controls.grade.value;
    this.currentStudent.combination = this.authForm.controls.combination.value;
  	this.sendCombi(this.currentStudent);
    this.isComplete = true;
  }

  sendCombi(value: Student) {
  	this.combinationService.sendCombi(value);
  }

  ngOnInit() {	
    this.authStatus = this.combinationService.isAuth;

    if(this.authStatus){

      this.combinationService.getCombi().subscribe(
      (data) => {
        this.currentStudent = data;
      },
      (error) => {
        console.log("Error "+error);
      });

      this.authForm = this.formBuilder.group({
        'firstname': [this.currentStudent.firstname],
        'surname': [this.currentStudent.surname],
        'grade': [this.currentStudent.grade],
        'combination': [this.currentStudent.combination]
      })
      this.isComplete = true;

    }else {
      this.authForm = this.formBuilder.group({
        'firstname': ['', Validators.required],
        'surname': ['', Validators.required],
        'grade': ['', Validators.required],
        'combination': ['', Validators.required]
      })
    }

  }
}

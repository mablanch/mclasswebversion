/**
* (c) Copyright 2019 CERN                                                     
*                                                                             
* This software is distributed under the terms of the GNU General Public      
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   
*                                                                             
* In applying this licence, CERN does not waive the privileges and immunities 
* granted to it by virtue of its status as an Intergovernmental Organization  
* or submit itself to any jurisdiction.                                       
*/

import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

export interface Student{
	firstname: String;
	surname: String;
	grade: String;
	combination: number;
}

@Injectable()
export class CombinationService {
	isAuth = false;

	//getCombination$: Observable<any>;
	private combinationSubject = new BehaviorSubject<Student>(null); // observable source

	sendCombi(value: Student){
		this.combinationSubject.next(value);
		this.isAuth = true;
	}

	getCombi(): Observable<any> {
		return this.combinationSubject.asObservable();
	}
}

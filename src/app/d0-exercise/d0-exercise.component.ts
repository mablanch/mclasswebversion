/**
* (c) Copyright 2019 CERN                                                     
*                                                                             
* This software is distributed under the terms of the GNU General Public      
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   
*                                                                             
* In applying this licence, CERN does not waive the privileges and immunities 
* granted to it by virtue of its status as an Intergovernmental Organization  
* or submit itself to any jurisdiction.                                       
*/

import { Component, OnInit, OnChanges, ElementRef, Input, ViewEncapsulation } from '@angular/core';
import { NgForm, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { D0, D0dataService } from '../services/d0data.service';
import { ToastrService } from 'ngx-toastr' ;
import * as d3 from 'd3';
import { regressionExp } from 'd3-regression';
import { nelderMead } from 'fmin';
import { MatProgressButtonOptions } from 'mat-progress-buttons';
import { NouiFormatter } from 'ng2-nouislider';
import { Observable, of } from 'rxjs';
import { MatButtonModule } from '@angular/material/button';
import { ModalComponent } from '../modal/modal.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export type DataType = {x:any, y:any};

@Component({
  selector: 'app-d0-exercise',
  templateUrl: './d0-exercise.component.html',
  styleUrls: ['./d0-exercise.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class D0ExerciseComponent implements OnInit {
  // charts 
  private url: string;
  private D0MassChart;
  private D0LifetimeChart;
  private D0Chart;
  private xScale;
  private yScale;
  private xScaleLT;
  private yScaleLT;
  private yAxis;
  private yAxisLT;
  private bins;
  private density;

  // variable ranges
  private minRangeGaus;
  private maxRangeGaus;
  private minRangeDistIP;
  private maxRangeDistIP;
  private minRangeDistPT;
  private maxRangeDistPT;
  private minRangeDistTAU;
  private maxRangeDistTAU;

  D0_data: D0[];
  D0_mass_coord;
  D0_decay_time;
  private sigArray: any[];
  private bkgArray: any[];
  private D0_MM: any[];

  private info;
  private fit;
  private propaBkg;
  interval: number;
  fitResult = [];
  meanLifetime = [];
  fitError;
  fitErrorMean;
  fitErrorSigma;
  valLifetime;
  errLifetime;

  initial_fit = [];
  fit_data;
  initial: Boolean = true;

  // boolean
  displayMass: boolean = true;
  displayFit: boolean = false;
  
  // forms
  public fitForm: FormGroup;
  public distForm: FormGroup;
  formBuilderFit = new FormBuilder;
  formBuilderDist = new FormBuilder;
  initRangeGauss = [ 1850, 1900];

  constructor(  private D0dataService: D0dataService, 
                private toastr: ToastrService, 
                public dialog: MatDialog ) { }

  /*
   notifications 
  */
  Info( e: string ) { 
    this.toastr.info(e); 
  }

  Success( e: string ) { 
    this.toastr.success(e); 
  }

  /*
    showdata(): load JSON file, initialise bins and plot D0 mass distribution
  */
  showdata() {
    this.url = 'assets/masterclass_data.json';
    this.D0dataService.getdata(this.url)
    .subscribe(
      (response) => {
        this.D0_data = response;
        this.initBins( this.D0_data, 0, 1810, 1920 );
        this.plotD0Mass(this.bins, ".histo_MM");
        this.displayMass = false;
      },
      (error) => {
        console.log("Error "+error);
      }
      );
  }

  /*
    onSubmitDist(distform : NgForm): submit form with min and max range variables input and display variable distributions
  */
  onSubmitDist( distform : NgForm ) {
    if ( this.displayFit ) {
      this.minRangeGaus = distform.form.controls.range.value[0];
      this.maxRangeGaus = distform.form.controls.range.value[1];

      this.minRangeDistIP = distform.form.controls.IP.value[0];
      this.maxRangeDistIP = distform.form.controls.IP.value[1];

      this.minRangeDistPT = distform.form.controls.PT.value[0];
      this.maxRangeDistPT = distform.form.controls.PT.value[1];

      this.minRangeDistTAU = distform.form.controls.TAU.value[0];
      this.maxRangeDistTAU = distform.form.controls.TAU.value[1];

      if( this.minRangeGaus == 1810 && this.maxRangeGaus == 1915 ){
        this.Info('You cannot set the signal range to be the same as the total masse range.');
      } else {
        this.plotDistribution();
      }
    } else {
      this.Info("You must fit mass distribution before.");
    }
  }

  /*
    initBins( data, index, minBin, maxBin ): initialise bins
  */
  initBins( data, index, minBin, maxBin ){
    this.bins = [];
    var coeff = 1;
    this.interval = (maxBin - minBin) / 110; // 110 bins

    for(let i = minBin; i < maxBin; i += this.interval){
      this.bins.push( [i, 0, 0]) //x0, candidates, probability
    }

    for (let i = 0; i < data.length; i++){
      if (this.density) {
        if (   ( data[i][1]/1000 <= this.maxRangeDistPT && data[i][1]/1000 >= this.minRangeDistPT ) 
             &&( data[i][2]*1000 <= this.maxRangeDistTAU && data[i][2]*1000 >= this.minRangeDistTAU )
             &&( Math.log10(data[i][3]) <= this.maxRangeDistIP && Math.log10(data[i][3]) >= this.minRangeDistIP )) {

          if (index == 1) coeff = 0.001; // for PT variable
          if (index == 2) coeff = 1000; // for TAU variable
          var item = data[i][index] * coeff;
          if (index == 3) item = Math.log10(item); // for IP variable

          var indexBin = Math.floor( (item - minBin) / this.interval );
          this.bins[ indexBin ][1]++;

          var mass = data[i][0];
          var indexProba = Math.floor( mass - 1810 );

          var probaBkg = this.density[ indexProba ][2];

          if( data[i][0] <= this.maxRangeGaus && data[i][0] >= this.minRangeGaus ){
            this.bins[ indexBin ][2] += probaBkg;
          } else {
            this.bins[ indexBin ][2] += 1; // background substraction
          }
        }
      } else {
        let item = data[i][index];
        var indexBin = Math.floor( (item - minBin)/this.interval );
        this.bins[ indexBin ][1]++;
      }
    }
  }

  /*
   plotD0Mass( data ): display D0 mass distribution
  */
  plotD0Mass( data, divName ) {
    var margin = {top: 20, right: 20, bottom: 40, left: 60},
        width = 560 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

    // Append SVG            
    var svg = d3.select(divName)
                .append("svg")
                .attr("width", "100%")
                .attr("height", "100%")
                .attr("viewBox", "0 0 "+width+" "+height);

    this.D0MassChart = svg.append("g")
        .attr('transform', `translate(${margin.left}, ${margin.top})`);

    // X axis    
    this.xScale = d3.scaleLinear<number>()
      .rangeRound([0, width - margin.left - 20])
      .domain([1810, 1920]);

    // Y axis: scale and draw
    this.yScale = d3.scaleLinear<number>().range([height - margin.left , 0]); 
    this.yAxis = this.D0MassChart.append('g').attr("class", "yAxix");   
    this.yScale.domain([0, 1700]);

    this.D0MassChart.append("g")
      .attr("transform", "translate(0," + (height - margin.bottom - 20) + ")")
        .call(d3.axisBottom(this.xScale))
        .append("text")
          .attr("transform", `translate(${2*width/3}, ${margin.bottom - 10})` )
             .attr("stroke", "black")
             .text("D0 Invariant Mass (MeV/c2)");

    this.D0MassChart.append("g")
      .call(d3.axisLeft(this.yScale))
        .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", "-5.1em")
          .attr("text-anchor", "end")
          .attr("stroke", "black")
          .text("D0 Candidates (0.5 MeV");

    this.D0MassChart.append("path");

    this.D0_mass_coord = [];

    this.D0MassChart.selectAll(".bar")
      .data(this.bins)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("transform", d => {
          return "translate(" + this.xScale(d[0]) + "," + this.yScale(d[1]) + ")";
       })
      .attr("x", 1)
      .attr("width", d =>  { return this.xScale(d[0]+1) - this.xScale(d[0]) - 1; })
      .attr("height", d =>  {
        if (d[1]>0) {
          var x = d[0] + this.interval/2;
          var y = d[1];
          var D0 = { x: x, y: y};
          this.D0_mass_coord.push(D0);
        } 
        return (height - margin.bottom - 20) - this.yScale(d[1]); })
      .attr("fill", "#4169E1");

    this.info = this.D0MassChart.insert("g").attr("class", "infoMath")
                        .append("text", "hist");
    this.fit = this.D0MassChart.append("path");
  }

  // usefull function ---------------------------------
  modelGaus(a, x) {
    function atleast_1d(x) {
    // Make sure that an object acts as an array (even if it's a scalar).
    if (typeof(x.length) === "undefined") {
      var tmp = new Array();
      tmp[0] = x;
      return tmp;
    }
    return x;
    }

    var i, j;
    var result = [];
    var sig2 = a[1]*a[1];
    var norm = a[0] / Math.sqrt(2*Math.PI*sig2); 
    x = atleast_1d(x);
    a = atleast_1d(a);
    for( i=0; i<x.length; i++){
      var diff = x[i]- a[2]; // x- mu
      result.push(norm * Math.exp(-0.5 * diff * diff /sig2));
    }

    //Background
    for( j=3; j<a.length; j++){ 
      for(i=0; i<x.length; i++){
        result[i] += a[j]*Math.pow(x[i], j-3);
      }
    }  
    return result;
  }

  /*
   fitD0mass( data ): fit mass distribution 
  */ 
  fitD0mass( data ) {
    var p0, p1, i, x, valchi2, do_fit;
    var xrange = [1814, 1914];
    var model = this.modelGaus;
    this.fit_data = [];

    function chi2 (p){
      valchi2 = [];
      var res = 0;
      if ( Math.abs(p[1]) > (xrange[1] - xrange[0]) || p[2] > xrange[1] || p[2] < xrange[0] ) {
        for (i = 0; i<data.length; i++ ) {
          valchi2.push(1e10);
        }
      }
      for (i=0; i<data.length; i++) {
        var observed = data[i].y;
        var expected = <any>model(p, data[i].x);
        if (observed >0) var tmp = observed - expected;
        valchi2.push( tmp );
        res += (valchi2[i] * valchi2[i]) / observed ;
      }
      return res;
    }
    var sumchi2 = chi2;

    do_fit = function (tab) {
      var i;33
      p0 = [24700, 10, 1800, 300];
      p1 = nelderMead(sumchi2, p0);
      for (let j = xrange[0]; j<=xrange[1]; j++){ // for each bin
        tab.push({x: j, y: model(p1.x, j)[0]})
      }
    }

    do_fit(this.fit_data);
    console.log(p1.x);
    return( p1.x ); 
  }

  /*
   fitter( data ): compute errors on mean and standart devation
   by ckecking chi2 width curve  
  */ 
  fitterGaus( data ) {
    var xrange = [1814, 1914];
    var model = this.modelGaus;

    function chi2 (x){
      var valchi2 = [];
      var res = 0;
      if ( Math.abs(p[1]) > (xrange[1] - xrange[0]) || p[2] > xrange[1] || p[2] < xrange[0] ) {
        for (var i = 0; i<data.length; i++ ) {
          valchi2.push(1e10);
        }
      }
      for (i=0; i<data.length; i++) {
        var observed = data[i].y;
        var expected = <any>model(x, data[i].x);
        if (observed >0) var tmp = observed - expected;
        valchi2.push( tmp );
        res += (valchi2[i] * valchi2[i]) / observed ;
      }
      return res;
    }

    // compute error on mean and standart deviation
    const p = this.fitD0mass( data )
    var chi2min = chi2(p);
    var npoints = 1000;

    var parinterval_mean = p[2] / (50 * npoints);
    var parinterval_sigma = p[1] / (50 * npoints);

    var parmin_mean = p[2] - parinterval_mean * npoints / 2;
    var parmin_sigma = p[1] - parinterval_sigma * npoints / 2;

    var parval_mean = [];
    var parval_sigma = [];

    var chi2val_mean = [];
    var chi2val_sigma = [];

    var params = p;
    for (var j = 0 ; j < npoints ; j++ ){
      parval_mean[j] = parmin_mean + j * parinterval_mean;
      params[2] = parval_mean[j];
      chi2val_mean[j] = { x: params[2], y: chi2( params ) - chi2min}
    }

    var params = this.fitD0mass( data )
    for (var j = 0 ; j < npoints ; j++ ){
      parval_sigma[j] = parmin_sigma + j * parinterval_sigma;
      params[1] = parval_sigma[j];
      chi2val_sigma[j] = { x: params[1], y: chi2( params ) - chi2min}
    }


    // ckecking width curve for mean
    var minval = 0;
    var maxval = 0;
    var k = 0;
    for(; k<npoints; ++k) {
      if(chi2val_mean[k].y <= 1){
        minval = chi2val_mean[k].x;
        break;
      } 
    }
    for(; k<npoints; ++k){
      if(chi2val_mean[k].y >= 1){
        maxval = chi2val_mean[k].x;
        break;
      } 
    }

    // ckecking width curve for standart dev
    var minval_sigma = 0;
    var maxval_sigma = 0;
    var k = 0;
    for(; k<npoints; ++k) {
      if(chi2val_sigma[k].y <= 1){
        minval_sigma = chi2val_sigma[k].x;
        break;
      } 
    }
    for(; k<npoints; ++k){
      if(chi2val_sigma[k].y >= 1){
        maxval_sigma = chi2val_sigma[k].x;
        break;
      } 
    }
    this.fitErrorMean = Math.abs(maxval - minval);
    this.fitErrorSigma = Math.abs(maxval_sigma - minval_sigma);
  }

  /*
    drawFitD0mass( data ): draw fitted curve 
  */
  drawFitD0mass( data ){
    function fNorm (x, mu, sigma, nSignal) {
      var ni1 = 1/(sigma*Math.sqrt(2*Math.PI)); 
      var ni2 = Math.exp(-1*((x-mu)*(x-mu))/(2*(sigma*sigma))); 
        return ( ni1*ni2*nSignal );
    }

    if( this.D0_mass_coord ){
      this.fitterGaus( data );

      var p1 = this.fitD0mass( data )
      this.D0MassChart.append("path")
                .datum(this.fit_data)
                  .attr("d",  d3.line<DataType>()
                      .x( d => { return this.xScale(d.x); })
                      .y( d => { return this.yScale(d.y); }))
                        .attr("fill", "#ff0000")
                        .attr("stroke", "#000")
                        .attr("fill-opacity","0")
                        .attr("stroke-width", 1.5)
                        .attr("stroke-linejoin", "round");

                        //.style("stroke-dasharray", ("3, 3"))
        // display initial fit
        if (this.initial) {
          this.initial_fit = this.fit_data;
          this.initial = false;
        }

        this.D0MassChart.append("path")
                .datum(this.initial_fit)
                  .attr("d",  d3.line<DataType>()
                      .x( d => { return this.xScale(d.x); })
                      .y( d => { return this.yScale(d.y); }))
                        .attr("stroke", "#000")
                        .attr("fill-opacity","0")
                        .attr("stroke-width", 0.5)
                        .style("stroke-dasharray", ("3, 3"));

        var total = 0;
        for (let j in data) total = total + data[j].y; 
        var background = Math.trunc(p1[3]) * data.length;
        var signal = total - background;

        this.density = [];
        for( var j = 0 ; j < this.bins.length ; j++){
          var x = this.bins[j][0];
          var res = p1[3] + fNorm(x, p1[2], p1[1], p1[0]);
          var probaBkg = p1[3] / res;
          this.density.push([x, res, probaBkg ])
        }

        var SEM = p1[1] / signal// Standart error of mean

        // Graphic part
       this.info // informations about fit (means, deviation, signal, background)
          .enter()
          .append("text")
            .merge(this.info)
            .attr('x', 270)
            .attr('y', 20)
            .style("font-size", "11px")
            .text("Total "+ (total)) // Entries: number of mass added
             .append("tspan")
              .attr('x', 270)
              .attr('y', 30)
              .text("Background : "+ background)
                  .append("tspan")
                  .attr('x', 270)
                  .attr('y', 40)
                  .text("Signal : "+ signal)
                    .append("tspan")
                    .attr('x', 270)
                    .attr('y', 50)
                    .text("Mean : "+ p1[2].toFixed(1)  + ' ± ' + this.fitErrorMean.toFixed(1))
                        .append("tspan")
                        .attr('x', 270)
                        .attr('y', 60)
                        .text("Deviation : "+ p1[1].toFixed(1) + ' ± ' + this.fitErrorSigma.toFixed(1));
      this.displayFit = true;
    } else {
      this.Info("You must plot D0 mass before.");
    }     
  }

  /*
   generateGaussian(): generate data from a Gaussian model for testing
  */
  generateGaussian( ) {
    var data = [];
    var x, y;
    var point;
    function fNorm (x, mu, sigma, nSignal) {
      var ni1 = 1/(sigma*Math.sqrt(2*Math.PI)); 
      var ni2 = Math.exp(-1*((x-mu)*(x-mu))/(2*(sigma*sigma))); 
        return ( ni1*ni2*nSignal );
    }
    for( var i=0; i<=100; i++){
      x = 1800 + i;
      y = 100 + fNorm (x, 1850, 7, 30000);
      point = { x: x, y: y};
      data.push(point);
    }
    return (data);
  }

  /*
    addLimitesGaus(): add thresholds for min and max signal range
  */
  addLimitesGaus() {
    var margin = {top: 20, right: 20, bottom: 40, left: 60},
        width = 560 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

    var line = this.D0MassChart.append("line")
      .attr("x1", d => { return this.xScale(this.maxRangeGaus)})
      .attr("x2", d =>{ return this.xScale(this.maxRangeGaus)})
      .attr("y1", 0)
      .attr("y2", height - 50)
      .attr("stroke-width", 1)
      .attr("stroke", "black")
      .attr("stroke-dasharray", "5")

    var line = this.D0MassChart.append("line")
      .attr("x1", d => { return this.xScale(this.minRangeGaus)})
      .attr("x2", d =>{ return this.xScale(this.minRangeGaus)})
      .attr("y1", 0)
      .attr("y2", height - 50)
      .attr("stroke-width", 1)
      .attr("stroke", "black")
      .attr("stroke-dasharray", "5")
  }

  /*  plotChart( data, minXdomain, maxXdomain, name, iddata, coeffvalues, xLegend , log10)
      plot variable chart
     
      data:               Array[][4] - data read from .json
      minXdomain:         number - minimum x-axis
      maxXdomain:         number - maximum x-axis
      name:               string - name of div allocated for the chart
      iddata:             number - index of line corresponding to variable in data
      coeffvalues:        number - variables PT and TAU need to be multiple by a coefficient
      xLegend:            string - x-axis legend
      log10:              boolean - true if variable is IP
  */
  plotChart( data, minXdomain, maxXdomain, name, xLegend ) {
    var margin = {top: 20, right: 20, bottom: 40, left: 60},
        width = 560 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

    this.xScale = d3.scaleLinear<number>()
      .rangeRound([0, width - margin.left - 20])
      .domain([minXdomain, maxXdomain]);

    // append SVG            
    var svg = d3.select(name)
                .append("svg")
                .attr("width", "100%")
                .attr("height", "100%")
                .attr("viewBox", "0 0 "+width+" "+height);

    this.D0Chart = svg.append("g").attr('transform', `translate(${margin.left}, ${margin.top})`);
    this.yScale = d3.scaleLog().range([height - margin.left, 0]);
    
    if ( name == ".histo_IP") {
      this.yScale.domain([.000001, .1]);
    } else {
      this.yScale.domain([.00001, 1]);
    }

    // draw y-axis
    this.yAxis = this.D0Chart.append('g').attr("class", "yAxix"); 

    // x-axis title
    this.D0Chart.append("g")
      .attr("transform", "translate(0," + (height - margin.bottom -20) + ")")
        .call(d3.axisBottom(this.xScale))
          .attr("text-anchor", "end")
             .append("text")
             .attr("transform", `translate(${2*width/3}, ${margin.bottom - 10})` )
             .attr("text-anchor", "end")
             .attr("stroke", "black")
             .text(xLegend);

    // y-axis title
    this.D0Chart.append("g")
      .call(d3.axisLeft(this.yScale))
        .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", "-5.1em")
          .attr("text-anchor", "end")
          .attr("stroke", "black")
          .text("D0 Candidates Fraction");
  
   // signal bar
   this.D0Chart.selectAll(".bar")
      .data(this.bins)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("transform", d => {
        if ( d[1]-d[2]>0 ) 
          return "translate(" + this.xScale(d[0]) + "," + this.yScale((d[1]-d[2])/data.length) + ")";
       })
      .attr("x", 1)
      .attr("width", d =>  { return this.xScale(d[0]+this.interval) - this.xScale(d[0]) - 1; })
      .attr("height", d =>  { 
        if ( d[1]-d[2]>0 )
          return (height - margin.bottom - 20) - this.yScale((d[1]-d[2])/data.length); }) 
      .attr("fill", "#4169E1");

   // background bar
   this.D0Chart.selectAll(".bar2")
      .data(this.bins)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("transform", d => {
        if ( d[2]>0 ) 
          return "translate(" + this.xScale(d[0]) + "," + this.yScale(d[2]/data.length) + ")";
       })
      .attr("x", 1)
      .attr("width", d =>  { return this.xScale(d[0]+this.interval) - this.xScale(d[0]) - 1; })
      .attr("height", d =>  { 
        if ( d[2]>0 )
          return (height - margin.bottom - 20) - this.yScale(d[2]/data.length); }) 
      .attr("fill", "#FF0000")
      .attr("fill-opacity","0.5");


    // signal legend
    var legend_sig = this.D0Chart.append('g')
                          .attr('class', 'legend')
                          .attr('transform', 'translate(' + (40 + 12) + ', 0)');
    legend_sig.append('rect')
            .attr('x', 250)
            .attr('y', 0)
            .attr('width', 12)
            .attr('height', 12)
            .attr('fill', "#4169E1");
    legend_sig.append('text')
            .text('Signal')
            .attr('x', 265)
            .attr('y', 10);

    // background legend
    var legend_bkg = this.D0Chart.append('g')
                          .attr('class', 'legend')
                          .attr('transform', 'translate(' + (40 + 12) + ', 0)');
    legend_bkg.append('rect')
            .attr('x', 250)
            .attr('y', 20)
            .attr('width', 12)
            .attr('height', 12)
            .attr("fill", "#FF0000")
            .attr("fill-opacity","0.5");
    legend_bkg.append('text')
            .text('Background')
            .attr('x', 265)
            .attr('y', 30);
  }

  /* plotDistribution(): display and update 6 histograms:
                              - related to variable PT, IP and TAU
                              - mass distribution
                              - TAU signal distribution and fit lifetime
                              - call ittedD0lifetime() to display scatterplot
     This function is link to button 'Plot distribution'
  */
  plotDistribution() {
    d3.selectAll(".histo_MM").selectAll("svg").remove();
    this.initBins( this.D0_data, 0, 1810, 1920 );
    this.plotD0Mass(this.bins, ".histo_MM");
    this.drawFitD0mass(this.D0_mass_coord);
    this.addLimitesGaus();

    d3.selectAll(".histo_PT").selectAll("svg").remove();
    this.initBins( this.D0_data, 1, 2, 20 );
    this.plotChart(this.D0_data, 2, 20, ".histo_PT", "D0 PT (GeV/c2)");

    d3.selectAll(".histo_lifetime").selectAll("svg").remove();
    this.initBins( this.D0_data, 2, 0, 11 );
    this.plotFitExpo();

    d3.selectAll(".histo_TAU").selectAll("svg").remove();
    this.initBins( this.D0_data, 2, 0, 11 );
    this.plotChart(this.D0_data, 0, 11, ".histo_TAU", "D0 decay time (ps)");

    d3.selectAll(".histo_IP").selectAll("svg").remove();
    this.initBins( this.D0_data, 3, -4, 2 );
    this.plotChart(this.D0_data, -4, 2, ".histo_IP", "log10(D0 IP (mm))");

    this.fittedD0lifetime();
  }

  /*
    fitDecayTime( data, minXdomain, maxXdomain, name ): create and display TAU signal distribution
  */
  fitDecayTime( data, minXdomain, maxXdomain, name ) {
    var margin = {top: 20, right: 20, bottom: 40, left: 60},
        width = 560 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

    this.xScaleLT = d3.scaleLinear<number>()
      .rangeRound([0, width - margin.left - 20])
      .domain([minXdomain, maxXdomain]);

    // Append SVG            
    var svg = d3.select(name)
                .append("svg")
                .attr("width", "100%")
                .attr("height", "100%")
                .attr("viewBox", "0 0 "+width+" "+height);

    this.D0LifetimeChart = svg.append("g")
      .attr('transform', `translate(${margin.left}, ${margin.top})`);

    // y-axis
     // scale different according variable
    this.yScaleLT = d3.scaleLog().domain([.1, 10000]).range ([height - margin.left, 0]);

     // draw y-axis
    this.yAxisLT = this.D0LifetimeChart.append('g').attr("class", "yAxix"); 

    // x-axis title
    this.D0LifetimeChart.append("g")
      .attr("transform", "translate(0," + (height - margin.bottom - 20) + ")")
        .call(d3.axisBottom(this.xScaleLT))
        .append("text")
          .attr("transform", `translate(${2*width/3}, ${margin.bottom - 10})` )
          .attr("stroke", "black")
          .text("D0 decay time (ps)");

    // y-axis title
    this.D0LifetimeChart.append("g")
      .call(d3.axisLeft(this.yScaleLT))
        .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", "-5.1em")
          .attr("text-anchor", "end")
          .attr("stroke", "black")
          .text("D0 Candidates Fraction");

    this.D0_decay_time = [];

   // signal bar
   this.D0LifetimeChart.selectAll(".bar")
      .data(this.bins)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("transform", d => {
        if ( d[1]-d[2]>0 ) 
          return "translate(" + this.xScaleLT(d[0]) + "," + this.yScaleLT((d[1]-d[2])) + ")";
       })
      .attr("x", 1)
      .attr("width", d =>  { return this.xScaleLT(d[0]+this.interval) - this.xScaleLT(d[0]) - 1; })
      .attr("height", d =>  { 
        if ( d[2]>0 ){
          var x = d[0] + this.interval/2;
          var y = d[1]-d[2];
          var D0 = { x: x, y: y};
          this.D0_decay_time.push(D0);
          if ( d[1]-d[2]>0 )
            return (height - margin.bottom - 20) - this.yScaleLT((d[1]-d[2])); 
        }
      }) 
      .attr("fill", "#0d742f");

    // signal legend
    var legend_sig = this.D0LifetimeChart.append('g')
                          .attr('class', 'legend')
                          .attr('transform', 'translate(' + (40 + 12) + ', 0)');
    legend_sig.append('rect')
            .attr('x', 250)
            .attr('y', 0)
            .attr('width', 12)
            .attr('height', 12)
            .attr('fill', "#0d742f");
    legend_sig.append('text')
            .text('Signal')
            .attr('x', 265)
            .attr('y', 10);

    // background legend
    var legend_bkg = this.D0LifetimeChart.append('g')
                          .attr('class', 'legend')
                          .attr('transform', 'translate(' + (40 + 12) + ', 0)');
    legend_bkg.append('rect')
            .attr('x', 250)
            .attr('y', 20)
            .attr('width', 12)
            .attr('height', 12)
            .attr("fill", "#000000")
            .attr("fill-opacity","0.8");
    legend_bkg.append('text')
            .text('Fit')
            .attr('x', 265)
            .attr('y', 30);
  }

  /*
   fitlifetime(data): fit lifetime with an exponential model
  */
  fitlifetime(data) {
    var p0, p1, i, x, fit_lifetime_data = [];

    // Fitting model: exponential
    var model = function (a, x) {
      var i, j;
      var result = []; 
      x = atleast_1d(x);
      a = atleast_1d(a);
      for( i=0; i<x.length; i++) result.push(a[0] * Math.exp( a[1] * x[i]));
      return result;
    }

    //data
    var xrange = [0.1, 10];

    // draw fit
    var draw_fit = function(p, tab) {
      for (var i = xrange[0]; i<=xrange[1]; i=i+0.1){ // for each bin
        tab.push({x: i, y:model(p, i)[0]})
      }
    }

    //do fit
    var do_fit = function (tab) {
      var i, chi, chi2, p0 = [5000, -2.5];

      // chi
      chi2 = function (p) {
        var i, chi = [];
        for (i=0; i<data.length; i++) {
          var observed = data[i].y;
          var expected = <any>( model(p, data[i].x) );
          var noise = Math.sqrt(observed);
          if ( noise>0 ) chi.push( (observed - expected) /  noise ); //chi.push( (data[i].y - <any>(model(p, data[i].x))) / Math.sqrt(data.y) );
        }
        var res = dot(chi, chi);
        return res;
      }
      p1 = nelderMead(chi2, p0);
      draw_fit(p1.x, tab);
    }

  var dot = function (a, b) {
        var i, result = 0.0;
        for (i = 0; i < a.length; i++) result += a[i] * b[i];
        return result;
  }

   function atleast_1d(x) {
        if (typeof(x.length) === "undefined") {
            var tmp = new Array();
            tmp[0] = x;
            return tmp;
        }
        return x;
    }

    do_fit(fit_lifetime_data);

    this.D0LifetimeChart.append("path")
            .datum(fit_lifetime_data) 
              .attr("d",  d3.line<DataType>()
                .defined((d) => { 
                   return d.y >= 0.1 ; 
                })
                  .x( d => { return this.xScaleLT(d.x); })
                  .y( d => { return this.yScaleLT(d.y); }))
                    .attr("fill", "#ff0000")
                    .attr("stroke", "#000")
                    .attr("fill-opacity","0")
                    .attr("stroke-width", 1.5)
                    .attr("stroke-linejoin", "round")

    this.fitResult[0] = p1.x[0];
    this.fitResult[1] = p1.x[1];

    this.valLifetime = -1/this.fitResult[1];

    this.fitterExpo( data );

    var func_exp = this.D0LifetimeChart.append('g')
                          .attr('class', 'legend')
                          .attr('transform', 'translate(' + (40 + 12) + ', 0)');
    func_exp.append('text')
            .style("font-size", "11px")
            .text("D0 lifetime "+ this.valLifetime.toFixed(3) + " ± " + this.errLifetime.toFixed(4))
            .attr('x', 50)
            .attr('y', 80);
  }

  /*
    plotFitExpo(): create, display TAU signal et fit lifetime
  */
  plotFitExpo() {
    this.fitDecayTime( this.D0_data, 0, 11, '.histo_lifetime');
    this.fitlifetime( this.D0_decay_time );
  }

  /*
    saveResultLifetime(): save lifetime value and error, and update scatterplot
      This function is link to 'Save result' button
  */
  saveResultLifetime() {
    if( this.D0LifetimeChart) {
      this.meanLifetime.push( [ this.maxRangeDistIP, this.valLifetime, this.errLifetime ] );
      this.fittedD0lifetime();
    } else {
      this.Info("You must plot distribution before.")
    }
    console.log(this.meanLifetime);
  }

  /*
    fittedD0lifetime(): create scatterplot 'Lifetime as IP cut'
  */
  fittedD0lifetime() {
    d3.selectAll(".scatterplot").selectAll("svg").remove();
    var margin = 150,
        width = 560,
        height = 400;

    var xScale = d3.scaleLinear<number>().rangeRound([0, width - margin]).domain([-4, 2]);

    // Append SVG            
    var svg = d3.select(".scatterplot")
                .append("svg")
                .attr("width", "100%")
                .attr("height", "100%")
                .attr("viewBox", "0 0 560 400");

    var chart = svg.append("g").attr("transform", "translate(" + 100 + "," + 100 + ")");

    // y-axis
     // scale different according variable
    var yScale = d3.scaleLinear().domain([0.35, 0.55]).range([height-margin, 0]);

     // draw y-axis
    var yAxis = chart.append('g').attr("class", "yAxix"); 

    // x-axis title
    chart.append("g")
      .attr("transform", "translate(0," + (height-margin) + ")")
        .call(d3.axisBottom(xScale))
        .append("text")
          .attr("y", height - 370)
          .attr("x", width - 200)
          .attr("text-anchor", "end")
          .attr("stroke", "black")
          .text("log10 D0 IP (mm)");

    // y-axis title
    chart.append("g")
      .call(d3.axisLeft(yScale))
        .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", "-5.1em")
          .attr("text-anchor", "end")
          .attr("stroke", "black")
          .text("D0 Candidates Fraction");

    // errorbars
    chart.append('g')
      .selectAll("line")
      .data(this.meanLifetime)
      .enter()
      .append("line")
        .attr("x1", function (d) { return xScale(d[0]); })
        .attr("x2", function (d) { return xScale(d[0]); })
        .attr("y1", function (d) { return yScale(d[1] - d[2]); })
        .attr("y2", function (d) { return yScale(d[1] + d[2]); })
        .attr("stroke", "black")
        .attr("stroke-width", 1.1);

    // add dots
    chart.append('g')
      .selectAll("dot")
      .data(this.meanLifetime)
      .enter()
      .append("circle")
        .attr("cx", d => { return xScale(d[0]); } )
        .attr("cy", d => { return yScale(d[1]); } )
        .attr("r", 2.5)
        .style("stroke", "black")
        .style("stroke-width", 1.1)
        .style("fill", "white");

    // PDG value - threshold
    var PDGvalue = 0.4101;
    chart.append("line")
      .attr('stroke', '#FF0000')
      .attr("x1", 0)
      .attr("y1", yScale(PDGvalue))
      .attr("x2", width - margin)
      .attr("y2", yScale(PDGvalue));

    chart.append("text")
          .attr("fill", "#FF0000")
          .attr("y", yScale(PDGvalue) + 10 )
          .attr("x", width - margin)
          .attr("text-anchor", "end")
          .style("font-size", "10px")
          .text("τ = (410.1 ± 1.5) × 10-15 s");
  }

  // resetExercise(): delete all charts and reinitialize fields
  /*resetExercise() {
    // delete charts
    d3.selectAll("svg").remove();
    d3.selectAll(".infoMath").remove();
    // reset 
    this.D0MassChart = null;
    this.fit = null;
    // reset boolean
    this.displayMass = true;
    this.displayFit = false;
    // reload component
    this.ngOnInit();
  }*/

  /*
    fitterExpo( data ): compute and draw chi2 as a function of p1 (with p1 from exp(p0+p1x) model)
  */
  fitterExpo( data ){
    var model = function (a, x) {
      var i, j;
      var result = []; 
      x = atleast_1d(x);
      a = atleast_1d(a);
      for( i=0; i<x.length; i++) result.push(a[0] * Math.exp( a[1] * x[i]));
      return result;
    }

    function atleast_1d(x) {
        if (typeof(x.length) === "undefined") {
            var tmp = new Array();
            tmp[0] = x;
            return tmp;
        }
        return x;
    }

    function chi2 ( data, f, p){
      var res = 0;
      for (var i=0 ; i<data.length ; i++){
        var observed = data[i].y;
        var expected = <any>( f(p, data[i].x) );
        if (observed >0) var tmp = Math.pow( (observed - expected), 2 ) / observed;
        res += tmp;
      }
      return res;
    }

    // chi2min
    var chi2min = chi2( data, model, this.fitResult);
    console.log("chi2 at fit value "+chi2min);

    var npoints = 1000;
    var parinterval = this.fitResult[1] / (50 * npoints);
    var parmin = this.fitResult[1] - parinterval * npoints / 2;
    var parval = [];
    var chi2val = [];
    var params = this.fitResult; 

    for (var i = 0 ; i < npoints ; i++ ){
      parval[i] = parmin + i * parinterval;
      params[1] = parval[i];
      chi2val[i] = { x: params[1], y: chi2( data, model, params) - chi2min}
    }

    // draw graph
    /*d3.selectAll(".fitter").selectAll("svg").remove();
    var margin = 150,
        width = 560,
        height = 400;

    var maxX = d3.max(chi2val, d => d.x) + 0.005;
    var minX = d3.min(chi2val, d => d.x) - 0.005;

    var maxY = d3.max(chi2val, d => d.y);
    var minY = d3.min(chi2val, d => d.y);

    var xScale = d3.scaleLinear<number>().rangeRound([0, width - margin]).domain([ minX, maxX ]);

    // Append SVG            
    var svg = d3.select(".fitter")
                .append("svg")
                .attr("width", "100%")
                .attr("height", "100%")
                .attr("viewBox", "0 0 560 400");

    var chart = svg.append("g").attr("transform", "translate(" + 100 + "," + 100 + ")");

    // y-axis
     // scale different according variable
    var yScale = d3.scaleLinear().domain([ minY, 1.3 ]).range([height-margin, 0]);

     // draw y-axis
    var yAxis = chart.append('g').attr("class", "yAxix"); 

    // x-axis title
    chart.append("g")
      .attr("transform", "translate(0," + (height-margin) + ")")
        .call(d3.axisBottom(xScale))
        .append("text")
          .attr("y", height - 370)
          .attr("x", width - 200)
          .attr("text-anchor", "end")
          .attr("stroke", "black")
          .text("p1");

    // y-axis title
    chart.append("g")
      .call(d3.axisLeft(yScale))
        .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", "-5.1em")
          .attr("text-anchor", "end")
          .attr("stroke", "black")
          .text("χ2");

    // add dots
    chart.append('g')
      .selectAll("dot")
      .data(chi2val)
      .enter()
      .append("circle")
        .attr("cx", d => { return xScale(d.x); } )
        .attr("cy", d => { return yScale(d.y); } )
        .attr("r", 0.5)
        .style("fill", "#000000");

    // PDG value - threshold
    chart.append("line")
      .attr('stroke', '#FF0000')
      .attr("x1", 0)
      .attr("y1", yScale(1))
      .attr("x2", width - margin)
      .attr("y2", yScale(1));
      */

    // ckecking width curve
    var minval = 0;
    var maxval = 0;
    var k = 0;
    for(; k<npoints; ++k) {
      if(chi2val[k].y <= 1){
        minval = chi2val[k].x;
        break;
      } 
    }
    for(; k<npoints; ++k){
      if(chi2val[k].y >= 1){
        maxval = chi2val[k].x;
        break;
      } 
    }
    this.fitError = Math.abs(maxval - minval);
    this.errLifetime = this.fitError / (this.fitResult[1] * this.fitResult[1]);

    console.log("CONSTANTE "+ (this.fitResult[0]) );
    console.log("SLOPE "+this.fitResult[1]);
    console.log("chi2 < 1 for "+minval+" param "+maxval);
    console.log("Error on param: "+ this.fitError);
  }

  /*
   openInstructions(): open instruction notification
  */
  openInstructions() {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '50%',
      data: {  modalTitle: this.titleInstructions, 
               modalText: this.textInstructions,
               addlogo:true}
    });
  }

  titleInstructions = 'Welcome to the LHCb masterclass exercise on measuring the lifetime of the D0 meson.'
  textInstructions = 
  'The goal of this exercise is to measure the lifetime of the D0 meson, a fundamental particle'
  +' made of a charm quark and an up anti-quark. In order to do so, you will first learn how to '
  +'separate signal D0 mesons from backgrounds. Finally, you will compare your results to the '
  +'values found by the Particle Data Group (http://pdgLive.lbl.gov).'
  +'\n\n'
  +'Step-by-step instructions :'
  +'\n'
  +'  1. Plot the D0 mass distribution. The mass of the D0 is a fundamendal variable which '
  +'separates signal (the peaking structure in the middle) from the flat background.'
  +'\n'
  +'  2. Read the results of the fit and use them to determine the signal range. The function '
  +'being fitted to the signal is a Gaussian, whose width, indicated by the greek letter σ, '
  +'is related to how far the signal extends from the mean for most probable) value. In '
  +'particular, an interval of ±1 σ around the mean value contains 68% of the signal, while'
  +' ±3 σ contains 99.7% of the signal. Use the slider to set the signal range to be ±3 σ '
  +' around the mean value.'
  +'\n'
  +'  3. Plot the variable distributions. You will see three further plots appearing, and in each '
  +'one the blue points represent the distribution of the signal in that variable while the red '
  +'points represent the distribution of the background. The plot is logarithmic in the Y axis, '
  +'and each point represents the fraction of the total signal in that bin. Which regions of '
  +'each variable contain mostly signal? Which contain mostly background ?'
  +'\n'
  +'  4. Fit the lifetime distribution. Save the results of your fit and compare them to the PDG' 
  +'value. Do they agree ?'
  +'  5. Repeat step 4 but now varying the upper D0 log(IP) variable range from 1.5 to -2 in '
  +'steps of 0.2. Do you notice a pattern?'
  +'Talk to a demonstrator about your results. Does the D0 lifetime with an log(IP) cut of' 
  +'-1.5 agree better or worse with the PDG than the lifetime with an log(IP) cut of 1.5 ?'

  /*
   principal function
  */ 
  ngOnInit() {
    // to test mass fit
    /*var data = {};
    data = this.generateGaussian();
    this.fitD0mass(data);
    */

    // forms initialisation
    this.fitForm = this.formBuilderFit.group({ 'range': [ [ 1810, 1915 ] ] });
    this.distForm = this.formBuilderDist.group({ 
                        'range': [ [ 1810, 1915 ] ],
                        'PT': [ [ 2.5, 20 ] ], 
                        'TAU': [ [ 0, 10 ] ],
                        'IP': [ [ -4, 1.5 ] ]})
  }
}
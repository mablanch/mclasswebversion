/**
* (c) Copyright 2019 CERN                                                     
*                                                                             
* This software is distributed under the terms of the GNU General Public      
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   
*                                                                             
* In applying this licence, CERN does not waive the privileges and immunities 
* granted to it by virtue of its status as an Intergovernmental Organization  
* or submit itself to any jurisdiction.                                       
*/

import { AfterViewInit, AfterContentInit, OnInit, Component, ElementRef, Input, ViewChild, HostListener } from '@angular/core';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { Particle, EventService } from '../services/event.service';
import { Student, CombinationService } from '../services/combination.service'
import { MatSlideToggleChange } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { ToastrService } from 'ngx-toastr' ;
import * as THREE from 'three';
import * as d3 from 'd3';
import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader'
import { NouiFormatter } from 'ng2-nouislider';
import { MatSliderModule } from '@angular/material/slider';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { ModalComponent } from '../modal/modal.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-event-display',
  templateUrl: './event-display.component.html',
  styleUrls: ['./event-display.component.css']
})
export class EventDisplayComponent implements OnInit {
  currentStudent = {} as Student;

  // scene properties
  private scene: THREE.Scene;
  private camera: THREE.PerspectiveCamera;
  private renderer: THREE.WebGLRenderer;
  private controls: OrbitControls;
  private autorotate: boolean = false;

  // slider toggle property
  checkedZoom: boolean = false; 
  checkedDetector: boolean = true;
  checkedHelp: boolean = false;
  changetoggle: MatSlideToggleChange;
  private help: any;

  // picking track properties
  private INTERSECTED: any;
  private INTERSECTEDprev: any;
  private INTERSECTED2: any;
  private mouse: THREE.Vector2;
  private mouse2: THREE.Vector2;
  private raycaster: THREE.Raycaster;
  private raycaster2: THREE.Raycaster;

  // 3d objects
  alllines: THREE.Object3D;
  detector: any;

  @ViewChild('canvas', {static: true}) canvasRef: ElementRef;
  private get canvas() : HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }

  // particle materials
  private pimaterial: THREE.LineBasicMaterial;
  private kmaterial: THREE.LineBasicMaterial;
  private mumaterial: THREE.LineBasicMaterial;
  private ematerial: THREE.LineBasicMaterial;

  // stage properties 
  public fieldOfView: number = 70;
  public nearClippingPane: number = 0.01;
  public farClippingPane: number = 8000;

  // event properties
  private theevent: Particle[];
  theparticle: Particle;
  private idparticle: number;
  eventnumber:number = 0;
  private path = 'assets/events/';
  filename: string;
  private url: string;

  // mass calculation variables
  private particleCount: number = 0; 
  myParticle1: Particle; // first particle saved
  myParticle2: Particle; // second particle saved
  massCalculated: number;
  firstMass:boolean = false;  

  // zoom/view properties
  private zoomOn: boolean = false;
  selectedView: string;

  // histogram properties
  @Input() private histogramdata:Array<number>;
  private margin:any;
  private chart:any;
  private svg:any;
  private width: number;
  private height: number;
  private data;
  private xScale: any;
  private yScale: any;
  private yAxis: any;
  private maxY: number;
  private bins;
  private infoHisto;

  constructor(  private eventService: EventService, 
                private toastr: ToastrService, 
                private combinationService: CombinationService,
                private sanitizer: DomSanitizer,
                public dialog: MatDialog  ) {}

  // notifications
  Warning(e: string) {
    this.toastr.warning(e);
  }
  Info(e: string) {
    this.toastr.info(e);
  }
  Error(e: string) {
    this.toastr.error(e);
  }
  Success(e: string) {
    this.toastr.success(e);
  }

  /*
    getStudentValues(): save name, grade and combination input in homepage
  */
  getStudentValues() {
    this.combinationService.getCombi().subscribe(
      (data) => {
        this.currentStudent = data;
      },
      (error) => {
        console.log("Error "+error);
      });
  }


  /* 
    showEvent(): load json file, put results in a Particle[] structure and call createEvent() to draw tracks
  */
 showEvent() {
    this.filename = 'event_'+this.currentStudent.combination+'_'+this.eventnumber+'.json';
    this.url = this.path + this.filename;

    this.eventService.getevent(this.url)
      .subscribe(
        (response) => {
          this.theevent = response;
          this.createEvent(this.theevent);
        },
        (error) => {
          console.log("Error "+error);
          this.Success('You have successfully completed MasterClassDisplay exercise.');
        }
        );
  }

  /*
    next(): allow to move on next event, load next json and draw tracks
  */
  next() {
    if ( this.eventnumber < 30 ) {
      this.eventnumber += 1;
      this.zoomReset();
      this.clearParticles();
      this.clearScene();
      this.clearInfo();
      this.showEvent(); 
      this.checkedZoom = false; // reset slider toggles
      this.checkedHelp = false;
    }   
  }

  /*
    previous(): allow to skip back to previous event, load previous json and draw tracks
  */
  previous() {
    if ( this.eventnumber >0 ) {
      this.eventnumber += -1;
      this.zoomReset();
      this.checkedZoom = false; // reset slider toggles
      this.checkedHelp = false;
      this.clearParticles();
      this.clearScene();
      this.clearInfo();
      this.showEvent();
    }
  }

  /*
   clearScene(): remove all tracks from scene
  */
  clearScene(){
    this.scene.remove(this.alllines);
  }

  /*
    saveParticle(selected: Particle): save two selected particles 
  */
  saveParticle(selected: Particle) {
    if( this.particleCount > 1 ){ // if two particles are already selected
      this.clearParticles();
    }
    if ( this.particleCount == 0 )
    {
      this.myParticle1 = selected;
      this.particleCount++;
    } else if ( selected == this.myParticle1 ) { // if the same particle is selected twice
      this.Warning('You already add this particle.');
    } else { // if second particle selected is different from the first one
      this.myParticle2 = selected;
      this.particleCount++
    }
  }

  /*
   calculate(): invariant mass calculation
  */
  calculate() {
    if ( this.myParticle1 && this.myParticle2) { 
      var a = (this.myParticle1.E + this.myParticle2.E)*(this.myParticle1.E + this.myParticle2.E);
      var b = (this.myParticle1.px + this.myParticle2.px)*(this.myParticle1.px + this.myParticle2.px);
      var c = (this.myParticle1.py + this.myParticle2.py)*(this.myParticle1.py + this.myParticle2.py);
      var d = (this.myParticle1.pz + this.myParticle2.pz)*(this.myParticle1.pz + this.myParticle2.pz);
      this.massCalculated = Math.sqrt(a-b-c-d);
    }else { 
      this.Info('You have to select two particles');
    } 
  }

  /*
   clearParticles(): clear variables related to mass calculation
  */
  clearParticles(){
    delete this.myParticle1;
    delete this.myParticle2;
    delete this.massCalculated;
    this.particleCount = 0;
  }

  /*
   clearInfo(): clear 'Particle information' fieldset
  */
  clearInfo() {
    delete this.theparticle;
  }

  massSaved = [];
  eventSaved = [];
  massAdded = [this.eventSaved, this.massSaved]

  /*
   add(): add mass calculated to histogram
  */
  add() {
    if ( this.myParticle1 || this.myParticle2 ) {
      if (this.massCalculated >= 1814 && this.massCalculated <= 1915 ) {
        if ( this.massAdded[0].indexOf(this.eventnumber) == -1 ) {
          this.eventSaved.push(this.eventnumber); // to avoid adding several values for each event
          this.massSaved.push(this.massCalculated);
          this.updateHisto(this.massAdded[1]);
          this.generateDownloadJsonUrl( this.massSaved ); // update JSON 
        } else {
          this.Error('You already added a mass for this event !');
        }
        } else {
          this.Error('Mass is not in range. Please, try again.');
        }
      } else {
        this.Info('You have to select two particles !')
      }
  }

  /* 
    createEvent(event: Particle[]): draw tracks from json data 
  */
  createEvent( event: Particle[] ){

    this.alllines = new THREE.Object3D; // 3D object which will contain all tracks
    
    // parse particle
    for ( let i in event ) {
      var track = event[i].trajectory;
      var name = event[i].particleId;
      var points = [];
      for ( let j in track ) {
        var point = track[j]; // coordinates x, y, z of a point
        points.push( new THREE.Vector3(1.0e-3*point[0], 1.0e-3*point[1], 1.0e-3*point[2]) ); // THREE.Vector3 is here a point in 3D space
      }
      var spline = new THREE.CatmullRomCurve3(points); // 3D spline curve from a series of points
      var vertex = spline.getPoints(200);
      var geometryLine = new THREE.BufferGeometry().setFromPoints( vertex );
            
      var line;
      // create final object to add to the scene 
      if (name=="pi+")
        line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x32CD32, linewidth: 1.1 } ), THREE.LineStrip );
      else if (name=="pi-")
        line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x4B0082, linewidth: 1.1 } ), THREE.LineStrip );
      else if (name=="K+" )
        line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x4169E1, linewidth: 1.1 } ), THREE.LineStrip );
      else if (name=="K-")
        line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0xFF4500, linewidth: 1.1 } ), THREE.LineStrip );
      else if (name=="D0")
        line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x999999, opacity: 0, transparent: true, linewidth: 0.1 } ), THREE.LineStrip );
      else 
        line = new THREE.Line( geometryLine, new THREE.LineBasicMaterial( { color: 0x999999, linewidth: 1 } ), THREE.LineStrip );

      this.alllines.add(line); // each track is add in a single object: alllines
    }
   this.scene.add( this.alllines );
   this.firstMass = true; // mass can be added to histogram
  }

  /* 
   createScene(): add scene, camera, renderer and controls 
  */
  createScene() {
    // Scene 
    this.scene = new THREE.Scene();

    // Camera
    let aspectRatio = this.getAspectRatio();
    this.camera = new THREE.PerspectiveCamera(
      this.fieldOfView,
      aspectRatio,
      this.nearClippingPane,
      this.farClippingPane
    );
    this.camera.position.set( -7.5, 7.5, 2.5 );
    this.camera.up.set( 0.0, 1.0, 0.0 );

    // Renderer 
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas, logarithmicDepthBuffer: true }); //logarithmicDepthBuffer: true
    this.renderer.setClearColor( 0xFFFFFF );
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);

    // Controls 
    this.controls = new OrbitControls( this.camera, this.renderer.domElement );
    this.controls.target.set( 0.0, 0.0, 9.0);
    this.controls.maxPolarAngle = 0.5*Math.PI;
    this.controls.autoRotate = this.autorotate;
    this.controls.update();
  }

  /*
   getAspectRatio(): retrun aspect ratio of the canvas
  */
  getAspectRatio() {
    return this.canvas.clientWidth / this.canvas.clientHeight;
  }

  /* 
   zoom(): zoom and center on collision
  */
  zoom() {
    this.zoomOn = true;
    const boundingBox = new THREE.Box3();
    boundingBox.setFromObject( this.alllines.children[0] ); // zoom on the fist particle (i.e D0)

    const center = new THREE.Vector3();
    boundingBox.getCenter( center );

    this.controls.reset();
    this.controls.target.set(center.x, center.y, center.z)

    this.camera.position.y = center.y;
    this.camera.position.x = center.x;

    this.camera.up.set( 1.0, 0.0, 0.0);
    this.camera.zoom = 4000;
    this.camera.updateProjectionMatrix();
    this.controls.update();
  }

  /*
   zoomReset(): replace scene and camera at their initial position
  */
  zoomReset() {
    this.zoomOn = false;
    this.camera.position.set( -7.5, 7.5, 2.5 );
    this.camera.up.set( 0.0, 1.0, 0.0 );
    this.controls.reset();
    this.controls.target.set( 0.0, 0.0, 8.0);
    this.controls.maxPolarAngle = 0.5*Math.PI;
    this.controls.update();
  }

  /*
   zoomChange(value: MatSlideToggleChange): if toggle is checked zoom() is called
  */
  zoomChange(value: MatSlideToggleChange) {
    this.checkedZoom = value.checked;
    if(value.checked) {
      this.zoom(); 
    } else {
      this.zoomReset();
    }
  }

  /* 
   createLight(): add one ambient ligth and two directional ligths 
  */
  createLight(){
    var ambientLight = new THREE.AmbientLight(0x040404);
    this.scene.add(ambientLight);

    var directionalLighta = new THREE.DirectionalLight(0xffffff);
    directionalLighta.position.set(1.0, 1.0, 1.0);
    this.scene.add(directionalLighta);

    var directionalLightb = new THREE.DirectionalLight(0xffffff);
    directionalLightb.position.set(-1.0, 1.0, -1.0);
    this.scene.add(directionalLightb);
  }

  /*
    loaddetector(): load GLTF object
  */
  loaddetector(){
    var loader = new GLTFLoader();
    var url = 'assets/lhcb.gltf';
    loader.load( url, this.onLoad);
  }

  /* 
    onLoad = (gltf: GLTF): add detector to the 3d scene
  */
  private onLoad = (gltf: GLTF) =>
  {
    this.detector = gltf.scene;
    this.detector.scale.set(0.001, 0.001, 0.001);
    this.setOpacityDetector(0.8)
    this.scene.add(this.detector);
  };

  /* 
   startRenderingLoop(): create a loop that causes the renderer to draw the scene every time the screen is refreshed 
  */
  startRenderingLoop() {
    var controls = this.controls;
    let component: EventDisplayComponent = this;
    (function render() {
      requestAnimationFrame(render);
      controls.update();
      component.renderer.render(component.scene, component.camera);
    }());
  }

  /* 
   onWindowResize(event): the render and camera resize the content of the canvas
  */
  onWindowResize(event) {
    this.canvas.style.width = '100%';
    this.canvas.style.height = '100%';
    this.camera.aspect = this.canvas.clientWidth / this.canvas.clientHeight;
    this.renderer.setSize(this.canvas.offsetWidth, this.canvas.offsetHeight);
    this.camera.updateProjectionMatrix(); 
  }


  /* 
   onMouseDown( event: MouseEvent): allow selection of tracks and mass calculation of two consecutive selection
  */
  public onMouseDown( event: MouseEvent) {
    event.preventDefault();
    var intersects;
    var zoomz = this.controls.target.distanceTo( this.controls.object.position ); // compute distance from camera to object 
    this.raycaster = new THREE.Raycaster();
    this.mouse = new THREE.Vector2();
    this.raycaster.linePrecision = zoomz / 150;

    if ( this.zoomOn ) this.raycaster.linePrecision = 0.000008; // when zoom is ON presicion need to be increased

    // compute mouse position with a coordinate system where the center of the screen is the origin
    this.mouse.x = ( ( event.clientX - this.renderer.domElement.offsetLeft ) / this.renderer.domElement.clientWidth ) * 2 - 1;
    this.mouse.y = - ( ( event.clientY - this.renderer.domElement.offsetTop ) / this.renderer.domElement.clientHeight ) * 2 + 1;

    this.raycaster.setFromCamera(this.mouse, this.camera); // update the picking ray with the camera and mouse position
    intersects = this.raycaster.intersectObjects( this.alllines.children ); // compute objects intersecting the picking ray

    if (intersects.length > 0) {
      if ( this.INTERSECTED != intersects[0].object ) {
        if (this.INTERSECTED ) this.INTERSECTED.material.linewidth = 1;
        this.INTERSECTED = intersects[0].object;
        this.INTERSECTED.material.linewidth = 4; // linewith of the current object increased from 1 to 4
      }else{ // line picked is the same as previous one
        this.INTERSECTED.material.linewidth = 1; 
      }
      this.saveParticle(this.theparticle); // particle picked is automatically saved
      if (this.myParticle1 && this.myParticle2) this.calculate(); // when two particles are saved invariant mass is calculated
    }
  }

  /* 
   onMouseMove( event: MouseEvent ):  highlight track when mouse move over scene 
  */
  public onMouseMove( event: MouseEvent ){
    if (this.alllines) {
      event.preventDefault();
      var intersects;
      var zoomz = this.controls.target.distanceTo( this.controls.object.position );
      this.raycaster2 = new THREE.Raycaster();
      this.mouse2 = new THREE.Vector2();
      this.raycaster2.linePrecision = zoomz / 150;

      if ( this.zoomOn ) this.raycaster2.linePrecision = 0.000008;

      this.mouse2.x = ( ( event.clientX - this.renderer.domElement.offsetLeft ) / this.renderer.domElement.clientWidth ) * 2 - 1;
      this.mouse2.y = - ( ( event.clientY - this.renderer.domElement.offsetTop ) / this.renderer.domElement.clientHeight ) * 2 + 1;

      this.raycaster2.setFromCamera(this.mouse2, this.camera);
      if ( this.alllines.children ) intersects = this.raycaster2.intersectObjects( this.alllines.children );

      if (intersects.length > 0) {
        if ( this.INTERSECTED2 != intersects[0].object ) {
          if (this.INTERSECTED2) this.INTERSECTED2.material.color.setHex(this.INTERSECTED2.currentHex);
          this.INTERSECTED2 = intersects[0].object;
          this.INTERSECTED2.currentHex = this.INTERSECTED2.material.color.getHex();
          this.INTERSECTED2.material.color.setHex(0xDEDE00); // color change
          var x = intersects[0].object;
          var idInit = this.alllines.children[0].id;
          this.idparticle = x.id - idInit;
          this.theparticle = this.theevent[this.idparticle]; // particle is saved in order to display its information
        }else{
          if (this.INTERSECTED2) this.INTERSECTED2.material.color.setHex(this.INTERSECTED2.currentHex);
          this.INTERSECTED2 = null;
        }
      }
    } 
  }

  /*
   onChangeView(): change camera position
  */
  public onChangeView(){
    if( this.selectedView == 'top') {
      this.camera.position.set( 0.0, 5.0, 8.5 );
      this.camera.up.set( 1.0, 0.0, 0.0 );
    }
    if( this.selectedView == 'side') {
      this.camera.position.set( -5.0, 0.0, 8.5 );
      this.camera.up.set( 0.0, 1.0, 0.0 );
    }
    if( this.selectedView == 'front') {
      this.camera.position.set( 0.0, 0.0, -10.0 );
      this.camera.up.set( 0.0, 1.0, 0.0 );
    }
    if( this.selectedView == 'perspective') {
      this.camera.position.set( -7.5, 7.5, 2.5 );
      this.camera.up.set( 0.0, 1.0, 0.0 );
    }
    this.camera.updateProjectionMatrix();
    this.controls.update();
  }

  /*
   onChangeOpacity(): react when slider value is modified 
  */
  public onChangeOpacity( event ) {
    this.setOpacityDetector(event.value); 
  }

  /*
   onChangeRotate( event ): react when checkbox is modified
  */
  public onChangeRotate( event ) {
    this.autorotate = event.checked;
    this.controls.autoRotate = this.autorotate;
  }

  /*
   setOpacityDetector( value ): change detector opacity
  */
  public setOpacityDetector( value ){
    this.detector.traverse( function (o) {
      if ( o.isMesh === true ) {
        o.material.transparent = true;
        o.material.opacity = value;
      }
    });
  }

  /*
   helpFindD0(): zoom on collision and highligth D0, pi- and K- by increasing their linewidth
  */
  helpFindD0() {
    this.zoom();
    this.help = this.alllines.children;
    this.help[0].material.transparent = false; //D0
    this.help[0].material.opacity = 1;
    this.help[0].material.linewidth = 4; 
    this.help[1].material.linewidth = 4; // K-
    this.help[2].material.linewidth = 4; // pi-
  }

  /*
   helpFindD0Reset(): reset zoom and put scene in its initial statement
  */
  helpFindD0Reset() {
    this.help = this.alllines.children;
    this.help[0].material.transparent = true; //D0
    this.help[0].material.opacity = 0;
    this.help[0].material.linewidth = 1.1; 
    this.help[1].material.linewidth = 1.1; // K-
    this.help[2].material.linewidth = 1.1; // pi-
  }

  /*
   helpD0Change(value: MatSlideToggleChange): if toogle 'Help' is switched function helpFindD0() is called
  */
  helpD0Change(value: MatSlideToggleChange) {
    this.checkedHelp = value.checked;
    if(value.checked) {
      this.helpFindD0(); 
    } else {
      this.helpFindD0Reset();
    }
  }

  /*
   hideDetChange(value: MatSlideToggleChange): display or remove detector from scene
  */
  hideDetChange(value: MatSlideToggleChange) {
    this.checkedDetector = value.checked;
    if(value.checked) {
      this.loaddetector(); 
    } else {
      this.scene.remove(this.detector);
    }
  }

  /*
   drawHisto(data): display of saved mass
  */
  drawHisto(data) {
    this.margin = {top: 20, right: 20, bottom: 40, left: 60};
    this.width = 560 - this.margin.left - this.margin.right;
    this.height = 450 - this.margin.top - this.margin.bottom;
    
    this.svg = d3.select('.histo')
                  .append('svg')
                  .attr('width', "100%" )
                  .attr('height', "100%" )
                  .attr("viewBox", "0 0 "+this.width+" "+this.height);

    // chart plot area
    this.chart = this.svg.append('g')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

    // X axis: scale and draw
    this.xScale = d3.scaleLinear<number>()
      .rangeRound([0, this.width - this.margin.left - 20])
      .domain([1810, 1920]);

    // get data
    let dom : number[] = this.xScale.domain();     
    let datagenerator = d3.histogram<number, number>()
      .domain([ dom[0], dom[1] ])
      .thresholds(this.xScale.ticks(20));

    let bins = datagenerator(data);

    // Y axis: scale and draw
    this.yScale = d3.scaleLinear<number>()
      .domain([0, 1])
      .range([this.height - this.margin.left , 0]); 

    this.yAxis = this.chart.append('g')
    .attr("class", "yAxix");
  
    let barWidth = this.xScale(bins[0].x1) - this.xScale(bins[0].x0) - 1;

    this.chart.selectAll(".bar")
      .data(bins)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("transform", d => {
        return "translate(" + this.xScale(d.x0) + "," + this.yScale((d.length)) + ")";
      })
      .attr("x", 1)
      .attr("width", barWidth)
      .attr("height", d =>  { return (this.height - this.margin.bottom - 20) - this.yScale(d.length); })
      .attr("fill", "#4169E1");

    this.chart.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + (this.height - this.margin.bottom - 20) + ")")
      .call(d3.axisBottom(this.xScale))
             .attr("text-anchor", "end")
             .append("text")
             .attr("transform", `translate(${this.width/2}, ${this.margin.bottom - 5})` )
             .attr("stroke", "black")
             .text("D0 Invariant Mass (MeV/c2)");

     this.yAxis
          .call(d3.axisLeft(this.yScale))
          .append("text")
             .attr("transform", "rotate(-90)")
             .attr("y", 10)
             .attr("dy", "-5.1em")
             .attr("text-anchor", "end")
             .attr("stroke", "black")
             .text("D0 Candidates (0.5 MeV");

      this.infoHisto = this.chart.insert("g").attr("class", "infoMath")
                        .append("text", "hist");         
  }

  /*
   updateHisto( data ): when a mass is added, histogram is updated
  */
  updateHisto( data ) {
    // get data
    let dom : number[] = this.xScale.domain();     
    let datagenerator = d3.histogram<number, number>()
      .domain([ dom[0], dom[1] ])
      .thresholds(this.xScale.ticks(20)); 

    let bins = datagenerator(data);

    // update yAxis
    this.yScale.domain([0, d3.max(bins, d => { return d.length; })]);
    this.yAxis.transition().duration(1000).call(d3.axisLeft(this.yScale))

    // update data 
    let barWidth = this.xScale(bins[0].x1) - this.xScale(bins[0].x0) - 1;

    this.chart.selectAll(".bar")
      .data(bins)
      .enter()
      .append("rect")
      .merge(this.chart.selectAll(".bar").data(bins))
      .transition()
      .duration(1000)
      .attr("class", "bar")
      .attr("transform", d => {
        return "translate(" + this.xScale(d.x0) + "," + this.yScale(d.length) + ")";
      })
      .attr("x", 1)
      .attr("width", barWidth)
      .attr("height", d =>  { return (this.height - this.margin.bottom - 20) - this.yScale(d.length); })
      .attr("fill", "#4169E1");

    if(d3.deviation(data)) var deviation = d3.deviation(data).toFixed(3);

    this.infoHisto
      .enter()
      .append("text")
        .merge(this.infoHisto)
        .attr("y", 50)
        .attr("x", 270)
        .text("Entries "+ data.length) // Entries: number of mass added
        .append("tspan")
          .attr("y", 65)
          .attr("x", 270)
          .text("Means: "+ d3.mean(data).toFixed(3))
          .append("tspan")
            .attr("y", 80)
            .attr("x", 270)
            .text("Std dev: "+ deviation);
  }

  /*
    generateDownloadJsonUrl( data ): allow to export/download JSON file formed of calculated masses
  */
  downloadJsonHref;
  generateDownloadJsonUrl( data ) {
    var theJSON = JSON.stringify( data );
    var nameJSON = "result_"+this.currentStudent.combination+".json";
    var url = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
    this.downloadJsonHref = url;
  }

  openInstructions() {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '70%',
      data: {}
    });
  }

  /*  
   principal function
  */
  public ngOnInit() {
    this.getStudentValues();
    this.createScene();
    this.loaddetector();
    this.createLight();
    this.showEvent();
    this.drawHisto(this.massAdded[1]);
    this.startRenderingLoop();
  }
}
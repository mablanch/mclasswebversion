import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})


export class ModalComponent implements OnInit {
  modalTitle: string;
  modalText: string;
  addlogo: Boolean

  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.modalTitle = this.data.modalTitle;
    this.modalText = this.data.modalText;
    this.addlogo = this.data.addlogo;
  }

}